using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocidad;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKey(KeyCode.W)))
        {
            transform.Translate(velocidad * Vector3.forward);
        }
        if ((Input.GetKey(KeyCode.A)))
        {
            transform.Translate(velocidad * Vector3.left);
        }
        if ((Input.GetKey(KeyCode.D)))
        {
            transform.Translate(velocidad * Vector3.right);
        }
        if ((Input.GetKey(KeyCode.S)))
        {
            transform.Translate(velocidad * Vector3.back);
        }
        if ((Input.GetKey(KeyCode.Space)))
        {
            transform.Translate(velocidad * Vector3.up);
        }
    }
}